# Copyright (c) 2016, Daniele Venzano
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Spark-Jupyter Zoe application description generator."""

import json
import sys
import os

APP_NAME = 'spark'
ZOE_APPLICATION_DESCRIPTION_VERSION = 3

options = {
    'master_mem_limit': {
        'value': 512 * (1024**2),
        'description': 'Spark Master memory limit (bytes)'
    },
    'worker_mem_limit': {
        'value': 12 * (1024**3),
        'description': 'Spark Worker memory limit (bytes)'
    },
    'submit_mem_limit': {
        'value': 12 * (1024**3),
        'description': 'Spark submit memory limit (bytes)'
    },
    'worker_cores': {
        'value': 6,
        'description': 'Cores used by each worker'
    },
    'worker_count': {
        'value': 2,
        'description': 'Number of workers'
    },
    'hdfs_namenode': {
        'value': 'hdfs-namenode.zoe',
        'description': 'Namenode hostname'
    },
    'submit_command': {
        'value': 'wordcount.py hdfs://192.168.45.157/datasets/gutenberg_big_2x.txt hdfs://192.168.45.157/tmp/wcount-out',
        'description': 'Spark submit command line'
    }
}

REPOSITORY = os.getenv("REPOSITORY", default="zapps")
VERSION = os.getenv("VERSION", default="latest")

MASTER_IMAGE = REPOSITORY + "/spark2-master-ts-predictor:" + VERSION
WORKER_IMAGE = REPOSITORY + "/spark2-worker-ts-predictor:" + VERSION
SUBMIT_IMAGE = REPOSITORY + "/spark2-submit-ts-predictor:" + VERSION


def spark_master_service(mem_limit):
    """
    :type mem_limit: int
    :rtype: dict
    """
    service = {
        'name': "spark-master",
        'image': MASTER_IMAGE,
        'monitor': False,
        'resources': {
            "memory": {
                "min": mem_limit,
                "max": mem_limit
            },
            "cores": {
                'min': 1,
                'max': 1
            }
        },
        'ports': [
            {
                'name': "Spark master web interface",
                'protocol': "tcp",
                'port_number': 8080,
                'url_template': "http://{ip_port}/",
            }
        ],
        'environment': [
            ["SPARK_MASTER_IP", "{dns_name#self}"],
            ["HADOOP_USER_NAME", "{user_name}"],
            ["PYTHONHASHSEED", "42"],
        ],
        'volumes': [],
        'total_count': 1,
        'essential_count': 1,
        'startup_order': 0,
        'replicas': 1,
        'command': None
    }
    return service


def spark_worker_service(count, mem_limit, cores):
    """
    :type count: int
    :type mem_limit: int
    :type cores: int
    :rtype List(dict)

    :param count: number of workers
    :param mem_limit: hard memory limit for workers
    :param cores: number of cores this worker should use
    :return: a list of service entries
    """
    worker_ram = mem_limit - (1024 ** 3) - (512 * 1025 ** 2)
    service = {
        'name': "spark-worker",
        'image': WORKER_IMAGE,
        'monitor': False,
        'resources': {
            "memory": {
                "min": mem_limit,
                "max": mem_limit
            },
            "cores": {
                'min': cores,
                'max': cores
            }
        },
        'ports': [],
        'environment': [
            ["SPARK_WORKER_CORES", str(cores)],
            ["SPARK_WORKER_RAM", str(worker_ram)],
            ["SPARK_MASTER_IP", "{dns_name#spark-master0}"],
            ["SPARK_LOCAL_IP", "{dns_name#self}"],
            ["PYTHONHASHSEED", "42"],
            ["HADOOP_USER_NAME", "{user_name}"]
        ],
        'volumes': [],
        'total_count': count,
        'essential_count': 1,
        'startup_order': 1,
        'replicas': 1,
        'command': None
    }
    return service


def spark_submit_service(mem_limit, worker_mem_limit, hdfs_namenode, command):
    """
    :type mem_limit: int
    :type worker_mem_limit: int
    :type hdfs_namenode: str
    :rtype: dict
    """
    executor_ram = worker_mem_limit - (1024 ** 3) - (512 * 1025 ** 2)
    driver_ram = (2 * 1024 ** 3)
    service = {
        'name': "spark-submit",
        'image': SUBMIT_IMAGE,
        'monitor': True,
        'resources': {
            "memory": {
                "min": mem_limit,
                "max": mem_limit
            },
            "cores": {
                'min': 2,
                'max': 2
            }
        },
        'ports': [],
        'environment': [
            ["SPARK_MASTER_IP", "{dns_name#spark-master0}"],
            ["SPARK_EXECUTOR_RAM", str(executor_ram)],
            ["SPARK_DRIVER_RAM", str(driver_ram)],
            ["HADOOP_USER_NAME", "{user_name}"],
            ["PYTHONHASHSEED", "42"],
            ['NAMENODE_HOST', hdfs_namenode]
        ],
        'volumes': [],
        'total_count': 1,
        'essential_count': 1,
        'startup_order': 2,
        'replicas': 1,
        'command': command
    }
    return service


if __name__ == '__main__':
    sp_master = spark_master_service(options['master_mem_limit']['value'])
    sp_worker = spark_worker_service(options['worker_count']['value'], options['worker_mem_limit']['value'], options['worker_cores']['value'])
    sp_submit = spark_submit_service(options['submit_mem_limit']['value'], options['worker_mem_limit']['value'], options['hdfs_namenode']['value'], options['submit_command']['value'])

    app = {
        'name': APP_NAME + "-submit",
        'version': ZOE_APPLICATION_DESCRIPTION_VERSION,
        'will_end': False,
        'size': 512,
        'services': [
            sp_master,
            sp_worker,
            sp_submit
        ]
    }

    json.dump(app, open("spark-submit.json", "w"), sort_keys=True, indent=4)

    print("One ZApps written")

